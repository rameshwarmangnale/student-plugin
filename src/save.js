const Save = ({ attributes }) => {
	const { groupName, isJoined } = attributes;

	return (
		<div>
			{isJoined ? (
				<p>You have joined the {groupName} group!</p>
			) : (
				<p>Join Students block - save function</p>
			)}
		</div>
	);
};

export default Save;
