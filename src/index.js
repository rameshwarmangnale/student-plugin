import { registerBlockType } from '@wordpress/blocks';
import './style.scss';
import Edit from './edit';
import Save from './save';

registerBlockType('blocks-course/student-group', {
	attributes: {
		groupName: {
			type: 'string',
			default: '',
		},
		newgroupName: {
			type: 'string',
			default: '',
		},
		isJoined: {
			type: 'boolean',
			default: false,
		},
	},
	edit: Edit,
	save: Save,
});
