<?php

/**
 * Plugin Name:       Student Group
 * Description:       Student Group
 * Requires at least: 5.7
 * Requires PHP:      7.0
 * Version:           0.1.0
 * Author:            Rameshwar Mangnale
 * Text Domain:       student-group
 * 
 * @package           blocks-course
 * 
 */
function blocks_course_student_group_block_init()
{
	register_block_type_from_metadata(__DIR__);
}
add_action('init', 'blocks_course_student_group_block_init');
